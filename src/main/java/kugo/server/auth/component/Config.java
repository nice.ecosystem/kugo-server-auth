package kugo.server.auth.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

@Component("cfg")
public class Config {
    @Autowired
    private NamedParameterJdbcTemplate jdbc1;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public String get(String key) {
        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("key", key);
        try {
            String sql = " SELECT cfg_val FROM config WHERE cfg_key=:key ";
            String val = jdbc1.queryForObject(sql, sqlParams, String.class);
            return val;
        } catch (Exception e) {
            log.error("Key "+key+" not found!");
            return null;
        }
    }
}
