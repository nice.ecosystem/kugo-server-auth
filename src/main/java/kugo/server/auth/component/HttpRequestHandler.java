package kugo.server.auth.component;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.lang.invoke.MethodHandles;

@Component
public class HttpRequestHandler {
    @Autowired
    private HttpServletRequest request;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public JSONObject getJsonRequest() throws Exception {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = request.getReader();
        String rawString = IOUtils.toString(reader);
        log.debug("Received request from: "+request.getRemoteAddr()+", URL: "+request.getRequestURL()+", string: "+rawString);
        JSONObject jsonReq = new JSONObject(rawString);
        return jsonReq;
    }

    public HttpServletRequest getRequest() {
        return request;
    }
}
