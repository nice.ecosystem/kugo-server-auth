package kugo.server.auth.component;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class DataUtil {
    @Autowired
    NamedParameterJdbcTemplate jdbc1;

    public JSONObject getMemberDetail(Long memberId) throws Exception {
        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("memberId", memberId);

        String sql = " SELECT * FROM member WHERE id=:memberId ";
        Map<String,Object> row = jdbc1.queryForMap(sql, sqlParams);

        return new JSONObject(row);
    }
}
