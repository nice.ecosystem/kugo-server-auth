package kugo.server.auth.component;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component("BasicAuth")
public class BasicAuth {
    @Autowired
    HttpRequestHandler requestHandler;
    @Autowired
    NamedParameterJdbcTemplate jdbc1;
    @Autowired
    Config cfg;
    @Autowired
    DataUtil dataUtil;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public JSONObject test() throws Exception {
        JSONObject jsonReq = requestHandler.getJsonRequest();
        return jsonReq;
    }

    public JSONObject signIn() throws Exception {
        JSONObject jsonReq = requestHandler.getJsonRequest();
        JSONObject jsonResp = new JSONObject();

        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("email", jsonReq.getString("email"));
        sqlParams.addValue("name", (jsonReq.has("name"))?jsonReq.getString("name"):null);
        sqlParams.addValue("photoUrl", (jsonReq.has("photoUrl"))?jsonReq.getString("photoUrl"):null);

        String sql = " SELECT * FROM member WHERE email=:email ";
        List<Map<String,Object>> rows = jdbc1.queryForList(sql, sqlParams);
        Long memberId;
        if (rows.size()<=0) {
            KeyHolder holder = new GeneratedKeyHolder();
            log.info("Member : "+jsonReq.getString("email")+" not found!");
            sql = " INSERT INTO member (email,name,photo_url) VALUES (:email,:name,:photoUrl) ";
            jdbc1.update(sql, sqlParams, holder);
            memberId = holder.getKey().longValue();

        } else {
            // member exists!
            log.info("Member : "+jsonReq.getString("email")+" exists!");
            memberId = (Long) rows.get(0).get("id");
        }

        String token = UUID.randomUUID().toString().toUpperCase();

        log.info("Member ID: "+memberId);

        jsonResp.put("success", true);
        jsonResp.put("memberId", memberId);
        jsonResp.put("token", token);
        jsonResp.put("email", jsonReq.getString("email"));
        jsonResp.put("detail", dataUtil.getMemberDetail(memberId));

        sqlParams.addValue("token", token);
        sqlParams.addValue("memberId", memberId);

        sql = " INSERT INTO access_token (token, member_id,expired_on) " +
                "  VALUES (:token, :memberId, ADDDATE(NOW(), INTERVAL CAST((SELECT cfg_val FROM config WHERE cfg_key='token.expiry.days') AS UNSIGNED) DAY)) ";

        jdbc1.update(sql, sqlParams);

        return jsonResp;
    }

    public JSONObject validateToken() throws Exception {
        JSONObject jsonReq = requestHandler.getJsonRequest();
        JSONObject jsonResp = new JSONObject();

        String token = jsonReq.getString("token");
        Long memberId = jsonReq.getLong("memberId");

        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("token", token);
        sqlParams.addValue("memberId", memberId);

        String sql = " SELECT COUNT(*) FROM access_token WHERE token=:token AND member_id=:memberId AND expired_on > NOW() ";
        boolean isValid = (jdbc1.queryForObject(sql, sqlParams, Integer.class)>0)?true:false;
        jsonResp.put("success", isValid);
        if (isValid) {
            jsonResp.put("detail", dataUtil.getMemberDetail(memberId));
        }

        return jsonResp;
    }

    public JSONObject logout() throws Exception {
        JSONObject jsonReq = requestHandler.getJsonRequest();
        JSONObject jsonResp = new JSONObject();

        jsonResp.put("success", true);

        return jsonResp;
    }

    public JSONObject validateSoftVersion() throws Exception {
        JSONObject jsonReq = requestHandler.getJsonRequest();
        JSONObject jsonResp = new JSONObject();

        int softVer = jsonReq.getInt("softVer");
        int minSoftVersion = Integer.parseInt(cfg.get("soft.ver.min"));

        boolean valid = (softVer < minSoftVersion)?false:true;

        jsonResp.put("success", valid);
        jsonResp.put("msg", (!valid)?"Versi aplikasi yang anda gunakan saat ini tidak valid, mohon untuk segera update aplikasi Anda!":null);
        jsonResp.put("updateUrl", (!valid)?cfg.get("soft.update.url"):null);

        return jsonResp;
    }

    public JSONObject updateFcmToken() throws Exception {
        JSONObject jsonReq = requestHandler.getJsonRequest();
        JSONObject jsonResp = new JSONObject();

        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("fcmToken", jsonReq.get("fcmToken"));
        sqlParams.addValue("memberId", jsonReq.get("memberId"));

        String sql = " UPDATE member SET fcm_token=:fcmToken WHERE id=:memberId ";
        jdbc1.update(sql, sqlParams);

        return jsonResp;
    }
}
