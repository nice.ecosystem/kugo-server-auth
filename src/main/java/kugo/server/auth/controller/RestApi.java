package kugo.server.auth.controller;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@RestController
public class RestApi {

    @Autowired
    ApplicationContext context;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @RequestMapping(value = {"/{className}/{methodName}.json"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> deduction(HttpServletRequest request) {
        JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", true);
        return defaultReturn(jsonResult);
        //return new ResponseEntity<String>("ANJENK", new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = {"/{className}/{methodName}.json"}, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> getComponent(HttpServletRequest request, @PathVariable("className") String className, @PathVariable("methodName") String methodName) {
        JSONObject jsonResult = new JSONObject();
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            String fullClassName = "kugo.server.auth.component."+className;
            Object bean = context.getBean(className);
            Class<?> c =  Class.forName(fullClassName);
            Method method = c.getMethod(methodName);
            jsonResult = (JSONObject) method.invoke(bean);
            log.info("API Result: "+jsonResult.toString());
        }
        catch (Exception e) {
            log.error("Class/method unknwon error: "+e,e);
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            jsonResult.put("success", false);
            jsonResult.put("msg", "Unknwon error!");
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<String>(jsonResult.toString(), headers, httpStatus);
    }

    private ResponseEntity defaultReturn(JSONObject jsonResult) {
        HttpHeaders headers = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.OK;
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<String>(jsonResult.toString(), headers, httpStatus);
    }

}
